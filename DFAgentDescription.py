import json
from iopa.ServiceDescription import ServiceDescription
from iopa.AID import AID

class DFAgentDescription():
    def __init__(self):
        self.__name = None
        self.services = []
        self.languages = []
    def getName(self):
        return self.__name

    def setName(self,name):
        self.__name=name
    def getServices(self):
        return self.services

    def addServices(self,service):
        self.services.append(service)
    def getLanguages(self):
        return self.services

    def addLanguages(self,language):
        self.languages.append(language)

    def asJSON(self):
        msg={}
        if self.__name:
            msg = {"Agent": self.__name.asJSON()}
        if self.services:
            v=[]
            for i in self.services:
                v.append(i.asJSON())
            msg["Services"]=v
        if self.languages:
            msg["Languages"]=self.languages
        return msg
    def asQuery(self):
        msg={}
        if self.services:
            msg["Description.Services.Type"]=str(self.services[0].asJSON()["Type"])
        if self.languages:
            msg["Description.Languages"]=str(self.languages[0])

        return msg
    def loadJSON(self,jsonstring):
        jsonstring=json.loads(json.dumps(jsonstring))
        if "Agent" in jsonstring:
            aid = AID()
            a = (jsonstring["Agent"])
            aid.loadJSON(a)
            self.setName(aid)
        if "Services" in jsonstring:
            services=[]
            for i in jsonstring["Services"]:
                s = ServiceDescription()
                s.loadJSON(i)
                services.append(s)
            self.services=services
        if "Languages" in jsonstring:
            for i in jsonstring["Languages"]:
                self.addLanguages(i)
