from iopa.ACLMessage import ACLMessage

class MessageTemplate:
    """
       Template for message matching
       """

    def __init__(self):
        self.performative = None
        self.sender = None
        self.receivers = []
        self.content = None
        self.language = None

    def match(self, message):
        if message.__class__ != ACLMessage:
            return False
        if (self.getPerformative() is not None):
            if (self.getPerformative() != message.getPerformative()):
                return False
        if (self.sender is not None):
            if message.sender is not None:
                if not message.sender.match(self.sender):
                    return False
            else:
                return False
        if (self.receivers != []):
            for tr in self.receivers:
                found = False
                for mr in message.receivers:
                    if mr.match(tr):
                        found = True
                        break
                if not found:
                    return False
        if (self.content is not None):
            if (self.content != message.content):
                return False
        if (self.getLanguage() is not None):
            if (self.getLanguage() != message.getLanguage()):
                return False
        return True

    def __and__ (self, other):
        """Implementation of & operator"""
        return ANDTemplate(self, other)

    def __or__(self, other):
        """Implementation of | operator"""
        return ORTemplate(self, other)

    def __invert__(self):
        """Implementation of ~ operator"""
        return NOTTemplate(self)


    def reset(self):
        self.__init__()

    def matchSender(self, sender):
        self.sender = sender

    def getSender(self):
        return self.sender

    def addReceiver(self, recv):
        self.receivers.append(recv)

    def removeReceiver(self, recv):
        if recv in self.receivers:
            self.receivers.remove(recv)

    def getReceivers(self):
        return self.receivers

    def matchPerformative(self, p):
        self.performative = p

    def getPerformative(self):
        return self.performative

    def matchContent(self, c):
        self.content = c

    def getContent(self):
        return self.content

    def matchLanguage(self, e):
        self.language = e

    def getLanguage(self):
        return self.language


class ANDTemplate(MessageTemplate):
    def __init__(self,expr1,expr2):
        self.expr1=expr1
        self.expr2=expr2

    def match(self, message):
        return (self.expr1.match(message) & self.expr2.match(message))

class ORTemplate(MessageTemplate):
    def __init__(self,expr1,expr2):
        self.expr1=expr1
        self.expr2=expr2

    def match(self, message):
        return (self.expr1.match(message) | self.expr2.match(message))

class NOTTemplate(MessageTemplate):
    def __init__(self, expr):
        self.expr = expr

    def match(self, message):
        return (not (self.expr.match(message)))