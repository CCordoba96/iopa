import os
import paho.mqtt.client as mqtt
import uuid
import logging
from iopa.ACLMessage import ACLMessage
from iopa.AID import AID
from iopa import Behaviour
import time
import json
from collections import deque
import sys
class Agent():
    def __init__(self,apikey,name):
        self._name = name
        self._apikey = apikey
        self.connect()
        self.finish = False
        self.registered = False
        self.activebehaviours = deque([])
        self.blockedbehaviours = deque([])
        self.services=[]
        self.messageQueue = deque([])
        self.aid = AID(self._name, self._apikey + "-" + self._name)
        self.register()
        self.start = False
        self.firstRun=True
        try:
            self.run()
        except KeyboardInterrupt:
            if self.registered == True:
                self.doDelete()
        except:
            print("Error inesperado: ", sys.exc_info()[0])
            self.doDelete()
            raise


    def addService(self,service):
        self.services.append(service)
    def getServices(self):
        return self.services
    def receive(self,template=None):
        if len(self.messageQueue)==0:
            return None
        else:
            if template is None:
                return self.messageQueue.popleft()
            else:
                lista=list(self.messageQueue)
                for msg in lista:
                    if template.match(msg):
                        self.messageQueue.remove(msg)
                        return msg
                        break
                return None

    def blockingReceive(self,template=None):
        if template is None:
            if (len(self.messageQueue) == 0):
                while len(self.messageQueue) == 0 & self.finish != True:
                    pass
                return self.messageQueue.popleft()
            else:
                return self.messageQueue.popleft()
        else:
            found = False
            while found==False & self.finish != True:
                if (len(self.messageQueue) != 0):
                    lista = list(self.messageQueue)
                    for message in lista:
                        if template.match(message):
                            self.messageQueue.remove(message)
                            return message
                            break
    def register(self):
        aclm=ACLMessage('inform')
        aclm.setSender(self.aid)
        aid=AID("AMS",self._apikey+"-AMS")
        aclm.addReceiver(aid)
        msg={"action":"register"}
        aclm.setContent(msg)
        self.send(aclm)
        while self.registered != True & self.finish==False:
            time.sleep(0.1)
    def deregister(self):
        print("Killing process...")
        aclm=ACLMessage('inform')
        aclm.setSender(self.aid)
        aid=AID("AMS",self._apikey+"-AMS")
        aclm.addReceiver(aid)
        msg={"action":"kill_agent"}
        aclm.setContent(msg)
        self.send(aclm)
        time.sleep(1)

    def myAppEventCallback(self,client, userdata,event):
        newaclm = ACLMessage(None, event.payload)
        content=newaclm.getContent()
        if(newaclm.getSender().getName()=="AMS"):
            if("action" in content):
                if (content["action"] == "end"):
                    print("Finalizando agente")
                    self.finish=True
            if(self.registered==False):
                if("register" in content):
                    if (content["register"] == "ok"):
                        print("Agente registrado correctamente")
                        self.registered = True
                    elif content["register"] == "no":
                        print("Agente registrado incorrectamente")
                        self.finish=True
                        self.registered=False

            elif 'action' in content:
                if(content["action"]=="start"):
                    print("Empezando agente")
                    self.start=True
                elif(content["action"]=="stop"):
                    self.start=False
                    print("Stop")
        else:
            self.messageQueue.append(newaclm)
            self.checkblocked()


    def _setup(self):
        raise NotImplementedError

    """
    Check when a message is received 
    """
    def checkblocked(self):
        if(len(self.blockedbehaviours)>0):
            size=len(self.blockedbehaviours)
            for i in range(0,size):
                ag=self.blockedbehaviours.popleft()
                ag.setNoBlocked()
                self.activebehaviours.append(ag)

    def send(self,aclmessage):
        if aclmessage.getReceivers() == []:
            raise Exception("No hay receptores en el mensaje")
        for aid in aclmessage.getReceivers():
            endpoint=aid.getAddress()
            newaclm=ACLMessage(aclmessage.getPerformative())
            newaclm.setSender(self.aid)
            newaclm.addReceiver(aid)
            newaclm.setLanguage(aclmessage.getLanguage())
            newaclm.setContent(aclmessage.getContent())
            diccio=newaclm.asJSON()
            
            self.appCli.publish(topic=endpoint, payload=json.dumps(diccio), qos=2)
            time.sleep(0.1)

    def getAID(self):
        return self.aid
    def getApikey(self):
        return self._apikey
    def run(self):
        self._setup()
        while self.finish != True:
            while len(self.activebehaviours)+ len(self.blockedbehaviours) !=0 | self.finish != True:
                if (self.registered & self.start ):
                    if(len(self.activebehaviours)>0):
                        a = self.activebehaviours.popleft()
                        # action
                        a.run()
                        if (a.done() == True):
                            a.onEnd()
                        elif (a.isBlocked() == True):
                            self.blockedbehaviours.append(a)
                        else:
                            self.activebehaviours.append(a)



                #comprobar done y flujo de vida de agente





    def doDelete(self):
        """
        Deberíamos Modificar takedown para que termine los comportamientos que tenemos
        :return:
        """
        print("Finishing agent")
        self.finish=True
        self.deregister()
    def getName(self):
        return self._name
    def connect(self,broker_address="52.90.162.132"):
        self.organization = "cac5px"
        self.deviceType = "broker"
        self.deviceId = "broker12345"
        self.appId = str(uuid.uuid4())

        self.appCli = mqtt.Client()
        self.appCli.connect(broker_address)
        self.appCli.on_message = self.myAppEventCallback
        
        self.appCli.subscribe(self._apikey+"-"+self._name)
        self.appCli.loop_start()
        """
            Pasarlo por linea de comandos o por archivo de configuracion o ambos
                :return: 
                """


    def addBehaviour(self,behaviour):
        behaviour.setAgent(self)
        self.activebehaviours.append(behaviour)



