import json
class AID():
    def __init__(self,name=None,addres=None):
        if name is not None:
            self.__name=name
        else:
            self.__name=""

        if addres is not None:
            self.address=addres
        else:
            self.address=""
    def getName(self):
        return self.__name

    def setName(self,name):
        self.__name=name
    def getAddress(self):
        return self.address

    def setAddress(self,name):
        self.address=name

    def setAddress(self,name,*api):
        if(len(api)!=0):
            self.__name=name
            self.address = str(api[0]) + "-" + str(name)
        else:
            self.address=name

    def match(self,other):
        """
        return True if two AIDs are similar
        else returns False
        :param other:
        :return:
        """
        if (other.getName() == self.__name) and other.getAddress() == self.getAddress():
            return True
        else:
            return False
    def __eq__(self,other):
        """
        return True if two AIDs are similar
        else returns False
        :param other:
        :return:
        """
        if other is None:
            return True
        elif other.getName == self.__name:
            return True
        else:
            return False
    def asJSON(self):
        if self.__name:
            msg = {"AID": self.__name}
        else:
            raise Exception("El agente no tiene nombre")
        if self.address:
            msg["Address"]=self.address
        else:
            raise Exception("El agente no tiene direccion")
        return msg
    def loadJSON(self,jsonstring):
        jsonstring=json.loads(json.dumps(jsonstring))
        if "AID" in jsonstring:
            self.setName(jsonstring["AID"])
        if "Address" in jsonstring:
            self.setAddress(jsonstring["Address"])
