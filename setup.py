from setuptools import setup

setup(

name="iopa",

version="0.1",

description="Librería de agentes para la plataforma PIoADE",

author="Carlos Cordoba Ruiz",

author_email="Carlos.Cordoba@alu.uclm.es",

url="https://pyioap.eu-gb.mybluemix.net/",

scripts=[], # Si queremos integrar scripts dentro del paquete, Ejemplo: scripts[‘script.py’]

packages=[] # Indicar los paquetes que van a formar parte de él, Paquete + Subpaquetes

)
