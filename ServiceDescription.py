import json
class ServiceDescription():
    def __init__(self,name=None,type=None):
        if name is not None:
            self.__name=name
        else:
            self.__name=""

        if type is not None:
            self.type=type
        else:
            self.address=""
    def getName(self):
        return self.__name

    def setName(self,name):
        self.__name=name
    def getType(self):
        return self.type

    def setType(self,type):
        self.type=type

    def match(self,other):
        """
        return True if two AIDs are similar
        else returns False
        :param other:
        :return:
        """
        if (other.getName() == self.__name) and other.getType() == self.getType():
            return True
        else:
            return False
    def __eq__(self,other):
        """
        return True if two Services are similar
        else returns False
        :param other:
        :return:
        """
        if other is None:
            return True
        elif other.getName() == self.__name and other.getType()==self.type:
            return True
        else:
            return False

    def asJSON(self):
        msg={}
        if self.__name:
            msg = {"Service": self.__name}
        if self.type:
            msg["Type"]=self.type
        else:
            raise Exception("No se ha especificado tipo de servicio")
        return msg
    def loadJSON(self,jsonstring):
        jsonstring=json.loads(json.dumps(jsonstring))
        if "Service" in jsonstring:
            self.setName(jsonstring["Service"])
        if "Type" in jsonstring:
            self.setType(jsonstring["Type"])
