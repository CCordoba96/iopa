from iopa.AID import AID
import json

class NotPerformative(Exception):
    def __init__(self,value):
        self.value=value
    def __str__(self):
        return "Error a la hora de seleccionar la performativa '"+str(self.value)+ "', no ha sido reconocida"

class ACLMessage():
    def __init__(self,performative=None,jsonstring=None):
        self._attrs={}
        #possible FIPA communicative acts
        self.commacts = ['accept-proposal', 'agree', 'cancel',
                         'cfp', 'call-for-proposal', 'confirm', 'disconfirm',
                         'failure', 'inform', 'not-understood',
                         'propose', 'query-if', 'query-ref',
                         'refuse', 'reject-proposal', 'request',
                         'request-when', 'request-whenever', 'subscribe',
                         'inform-if', 'proxy', 'propagate']
        if performative is not None:
            if performative in self.commacts:
                self._attrs["performative"]=performative.lower()
            else:
                raise NotPerformative(performative)

        self.sender=None
        self.receivers=[]
        self.content=None
        self.language=None
        if jsonstring:
            self.loadJSON(jsonstring)

    def setLanguage(self,language):
        self.language=str(language)

    def getLanguage(self):
        return self.language
    def setSender(self,sender):
        self.sender=sender
    def getSender(self):
        return self.sender

    def addReceiver(self,recv):
        """
        set the sender (AID)
        :param recv:
        :return:
        """
        self.receivers.append(recv)

    def removeReceiver(self,recv):
        """
        removes a receiver from the list (AID)
        :param recv:
        :return:
        """
        if recv in self.receivers:
            self.receivers.remove(recv)

    def getReceivers(self):
        return self.receivers

    def setPerformative(self, p):
        """
        sets the message performative (string)
        must be in ACLMessage.commacts
        """
        # we do not check if is a fipa performative
        # any string is valid...
        #if p and (p.lower() in self.commacts):
        if p in self.commacts:
            self._attrs["performative"] = str(p.lower())
        else:
            raise Exception("Performativa no valida")

    def getPerformative(self):
        """
        returns the message performative (string)
        """

        return (self._attrs['performative'])

    def setContent(self, c):
        """
        sets the message content (string, bytestream, ...)
        """
        self.content = c
    def getContent(self):
        return self.content


    def asJSON(self):
        """
               returns a JSON version of the message
               """
        p={}
        p["performative"]=str(self.getPerformative())
        if self.sender:
            p["sender"]=self.sender.asJSON()
        if self.language:
            p["language"]=self.getLanguage()
        if self.receivers:
            v=[]
            for i in self.receivers:
                v.append(i.asJSON())
            p["receivers"]=v
        if self.content:
            if type(self.content) is dict:
                p["content"] = json.dumps(self.content)
            else:
                p["content"] = self.content

        return p

    def createReply(self):
        m=ACLMessage()
        m.setPerformative(self.getPerformative())
        m.addReceiver(self.getSender())
        return m
    def loadJSON(self, jsonstring):
        """
        Lee un archivo Json que recibimos
        :param jsonstring:
        :return:
        """
        p = json.loads(jsonstring)

        if "performative" in p:
            self.setPerformative(p['performative'])
        if "language" in p:
            self.setLanguage(p["language"])
        if "sender" in p:
            aid=AID()
            a=(p["sender"])
            aid.loadJSON(a)
            self.setSender(aid)
        if "receivers" in p:
            for i in p["receivers"]:
                s = AID()
                s.loadJSON(i)
                self.addReceiver(s)
        if "content" in p:
            try:
                self.setContent(json.loads(p["content"]))
            except:
                self.setContent(p["content"])

