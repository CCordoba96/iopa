
class Behaviour():
    def __init__(self):
        self._running=False
        self.firstrun=True
        self.blocked=False

    def setAgent(self,agent):
        self.myAgent=agent

    def send(self,aclm):
        self.myAgent.send(aclm)

    def onStart(self):
        pass

    def onEnd(self):
        pass



    def action(self):
        """
        Main loop of the behaviour
        must be overriden
        :return:
        """
        raise NotImplementedError
    def isBlocked(self):
        return self.blocked
    def setNoBlocked(self):
        self.blocked=False
    def block(self):
        self.blocked=True

    def run(self):
        if(self.firstrun):
            self.onStart()
            self.firstrun=False
        self.action()

    def receive(self, template=None):
        if template is None:
            return self.myAgent.receive()
        else:
            return self.myAgent.receive(template)

    def blockingReceive(self,template=None):
        if template is not None:
            self.template=template
            return self.myAgent.blockingReceive(template=template)
        else:
            return self.myAgent.blockingReceive()

    def end(self):
        self.finish=True
    def finish(self):
        return self.finish

    def done(self):
        """
        return if the behaviour has finished
        :return:
        """
        return False
