from iopa.AID import AID
from iopa.ACLMessage import ACLMessage
from iopa.MessageTemplate import MessageTemplate
from iopa.DFAgentDescription import DFAgentDescription

class DFService():
    def __init__(self):
        self.agent=None

    def _generatemsg(self):
        aclm = ACLMessage('inform')
        aclm.setSender(self.agent.getAID())
        aid = AID("DF", self.agent.getApikey() + "-DF")
        aclm.addReceiver(aid)
        return aclm

    def register(self,agent=None,descripcion=None):
        if agent is None:
            raise Exception("No se ha especificado agente")
        if descripcion is None:
            raise Exception("No se ha especificado descripcion")
        if self.agent==None:
            self.agent=agent
        aclm=self._generatemsg()
        msg = {"action": "register"}
        msg["Description"]=descripcion.asJSON()
        aclm.setContent(msg)
        self.agent.send(aclm)

    def deregister(self):
        if self.agent is None:
            raise Exception("No se ha registrado agente antes")
        aclm=self._generatemsg()
        msg = {"action": "deregister"}
        aclm.setContent(msg)
        self.agent.send(aclm)
    def search(self,agent=None,descripcion=None):
        if agent is None:
            raise Exception("No se ha especificado agente")
        if descripcion is None:
            raise Exception("No se ha especificado descripcion")
        if self.agent==None:
            self.agent=agent
        aclm=self._generatemsg()
        msg = {"action": "search"}
        msg["Description"]=descripcion.asJSON()
        aclm.setContent(msg)
        self.agent.send(aclm)
        mt = MessageTemplate()
        aid = AID()
        aid.setAddress("DF", "a")
        mt.matchSender(aid)
        msg=self.agent.blockingReceive(mt)
        list=msg.getContent()["search"]
        descriptions=[]
        for i in list:
            df=DFAgentDescription()
            df.loadJSON(i["Description"])
            descriptions.append(df)
        return descriptions
